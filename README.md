# unsplash-demo



## Introduction

Unsplash-Demo  is an iOS Application that lets you search for photos using the Unsplash API


## Framework Used
1. UIKit


## Caching Mechanism Used
1. URLCache

I used the following caching mechanism as my data storage because it is what I using in my projects to cater offline data. URLCache by default is already saving all the response we receive and it is just kept inside our iPhone, by using a correct URLSessionConfiguration your urlsession will give the last saved data requested when you still have active connections.

 It is also handy to use urlcache with Etag.

```Swift
let configuration = URLSessionConfiguration.default
configuration.httpCookieAcceptPolicy = .always
configuration.urlCache?.memoryCapacity = 50_000_000
configuration.urlCache?.diskCapacity = 100_000_000
return configuration
```

    
2. NSCache 

```Swift
    let imageCache = NSCache<AnyObject, AnyObject>() 
```

## API Used
1. Unsplash API
