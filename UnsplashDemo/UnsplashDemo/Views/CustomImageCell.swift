import Foundation
import UIKit

final class CustomImageCell: UITableViewCell {
    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.accessibilityLabel = "Description"
        return label
    }()
    
    let searchedImage: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.accessibilityLabel = "Image"
        return imageView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(searchedImage)
        
        self.isAccessibilityElement = false
        self.accessibilityElements = [descriptionLabel, searchedImage]
        
        NSLayoutConstraint.activate([
            descriptionLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            descriptionLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16),
            descriptionLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -16),
            
            searchedImage.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            searchedImage.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            searchedImage.widthAnchor.constraint(equalToConstant: 50),
            searchedImage.heightAnchor.constraint(equalToConstant: 50),
            
            descriptionLabel.trailingAnchor.constraint(lessThanOrEqualTo: searchedImage.leadingAnchor, constant: -16)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupPhotoItem(photoItem: Photo) {
        if let description = photoItem.description, let altDescription = photoItem.altDescription {
            self.descriptionLabel.text = description.isEmpty ? altDescription : description
        } else {
            self.descriptionLabel.text = "No description"
        }
        
        Task {
            self.searchedImage.image = try await Services.shared.searchPhotoService.downloadImage(imageURL: photoItem.urls.full)
        }
    }
}
