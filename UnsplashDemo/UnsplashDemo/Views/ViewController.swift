import UIKit

class ViewController: UIViewController {

    private var searchTextField: UITextField = {
        let textField = UITextField(frame: .zero)
        textField.placeholder = "Search"
        textField.borderStyle = .roundedRect
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private var searchButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Search", for: .normal)
        button.backgroundColor = .systemGreen
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 5
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private var tableView: UITableView = {
        return UITableView(frame: .zero, style: .plain)
    }()
    
    
    private lazy var dataSource = makeDataSource()
    private lazy var spinner = UIActivityIndicatorView(style: .large)
    
    private var searchResults: [Photo] = []
    
    enum Section {
        case main
    }
    
    typealias DataSource = UITableViewDiffableDataSource<Section, Photo>
    typealias Snapshot = NSDiffableDataSourceSnapshot<Section, Photo>
    
    // MARK: - Lifecycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationbar()
        setupSearchTextField()
        setupSearchButton()
        setupTableView()
        setupSpinner()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: - TableView Methods
    
    func applySnapshot(animatingDifferences: Bool = true) {
        var snapshot = Snapshot()
        snapshot.appendSections([.main])
        snapshot.appendItems(searchResults)
        
        dataSource.apply(snapshot)
    }
    
    func makeDataSource() -> DataSource {
        let dataSource = DataSource(tableView: tableView, cellProvider: { (tableView, indexPath, title) -> UITableViewCell? in
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCell", for: indexPath) as? CustomImageCell else {
                return UITableViewCell()
            }
            
            cell.setupPhotoItem(photoItem: self.searchResults[indexPath.row])
            return cell
        })
        
        return dataSource
    }
    
    // MARK: - Private Methods
    
    private func setupNavigationbar() {
        self.title = "Search Photos"
        self.navigationItem.largeTitleDisplayMode = .automatic
    }
    
    private func setupSearchTextField() {
        view.addSubview(searchTextField)
        
        searchTextField.text = "Ferrari"
        searchTextField.becomeFirstResponder()
        
        NSLayoutConstraint.activate([
            searchTextField.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20),
            searchTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            searchTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            searchTextField.heightAnchor.constraint(equalToConstant: 40),
        ])
    }

    private func setupSearchButton() {
        view.addSubview(searchButton)
        
        searchButton.addTarget(self, action: #selector(searchButtonClicked), for: .touchUpInside)
        
        NSLayoutConstraint.activate([
            searchButton.topAnchor.constraint(equalTo: searchTextField.bottomAnchor, constant: 10),
            searchButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            searchButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            searchButton.heightAnchor.constraint(equalToConstant: 50),
        ])
    }
    
    private func setupTableView() {
        view.addSubview(searchButton)
        
        tableView.register(CustomImageCell.self, forCellReuseIdentifier: "CustomCell")
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: searchButton.bottomAnchor, constant: 20),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
    }
    
    private func setupSpinner() {
        spinner.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(spinner)
        
        NSLayoutConstraint.activate([
            spinner.topAnchor.constraint(equalTo: searchButton.bottomAnchor, constant: 10),
            spinner.centerXAnchor.constraint(equalTo: searchButton.centerXAnchor)
        ])
    }
    
    // MARK: - Testing API Call
    
    @objc func searchButtonClicked(_ sender: Any) {
        showSpinner(state: true)
        self.searchTextField.resignFirstResponder()
        
        guard let search = searchTextField.text else {
            return
        }

        Task {
            do {
                let photos = try await Services.shared.searchPhotoService.searchPhoto(searchText: search)
                print("🟢 RESULT COUNT: \(photos.count)")
                
                self.searchResults.removeAll()
                self.searchResults.append(contentsOf: photos)
                applySnapshot()
                
                showSpinner(state: false)
            } catch {
                print("🔴 ERROR: \(error)")
                showSpinner(state: false)
            }
        }
    }
    
    private func showSpinner(state: Bool) {
        state ? spinner.startAnimating() : spinner.stopAnimating()
        tableView.isHidden = state
        searchButton.isHidden = state
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedPhoto = self.searchResults[indexPath.row]
        let detailViewController = DetailViewController(photo: selectedPhoto)
        self.navigationController?.pushViewController(detailViewController, animated: true)
    }
}
