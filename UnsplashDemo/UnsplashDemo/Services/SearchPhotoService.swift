import Foundation
import UIKit

protocol SearchPhotoServiceProtocol {
    func searchPhoto(searchText: String)
    func downloadImage(imageURL: String) async -> UIImage?
}

final class SearchPhotoService {
    let endpoint = "https://api.unsplash.com/search/photos"
    let clientID = "NHr5nmnvy4fJA0AtfpReQm_EI2SBnnvPajDObRtmYbY"
    
    private let httpClient: HTTPClient
    
    let imageCache = NSCache<AnyObject, AnyObject>()
    
    init(httpClient: HTTPClient) {
        self.httpClient = httpClient
    }
    
    func searchPhoto(searchText: String)  async throws -> [Photo] {
        guard var urlComponents = URLComponents(string: endpoint) else {
            throw HTTPClientError.invalidURL
        }
        
        urlComponents.queryItems = [
            URLQueryItem(name: "query", value: searchText),
            URLQueryItem(name: "client_id", value: clientID)
        ]
        
        guard let url = urlComponents.url else {
            throw HTTPClientError.invalidURL
        }
        
        
        do {
            let result = try await self.httpClient.request(url: url, method: HTTPMethod.GET, body: nil)
            let searchPhotosDTO = try JSONDecoder().decode(SearchPhotosDTO.self, from: result.body)
            return searchPhotosDTO.results
        } catch {
            throw error
        }
    }
    
    func downloadImage(imageURL: String) async throws -> UIImage? {
        if let imageFromCache = imageCache.object(forKey: imageURL as AnyObject) as? UIImage {
            return imageFromCache
        }
        
        if let url = URL(string: imageURL) {
            let data = try? await self.httpClient.request(url: url, method: HTTPMethod.GET, body: nil)
            if let result = data, let img = UIImage(data: result.body) {
                let imageToCache = img.resizedToFitSquare(sideLength: 300)
                self.imageCache.setObject(imageToCache!, forKey: imageURL as AnyObject)
                return imageToCache
            }
        }
        
        return nil
    }
    
    private func resizeImage(image: UIImage, newHeight: CGFloat) -> UIImage {
        let scale = newHeight / image.size.height
        let newWidth = image.size.width * scale
        UIGraphicsBeginImageContext(CGSize(width:newWidth, height:newHeight))
        image.draw(in:CGRect(x:0, y:0, width:newWidth, height:newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}
