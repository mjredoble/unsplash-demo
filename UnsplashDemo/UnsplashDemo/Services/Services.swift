import Foundation

final class Services {
    static let shared = Services()
    
    private init() {}
    lazy var httpClient = HTTPClient()
    lazy var searchPhotoService = SearchPhotoService(httpClient: httpClient)    
}
