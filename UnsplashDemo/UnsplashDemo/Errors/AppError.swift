struct AppError: Error {
    let key: String
    let description: String?
}
