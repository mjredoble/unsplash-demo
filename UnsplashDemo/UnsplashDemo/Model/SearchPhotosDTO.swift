import Foundation
import UIKit

// MARK: - SearchPhotosResponse
struct SearchPhotosDTO: Codable {
    let total: Int
    let totalPages: Int
    let results: [Photo]
    
    enum CodingKeys: String, CodingKey {
        case total
        case totalPages = "total_pages"
        case results
    }
}

// MARK: - Photo
struct Photo: Codable, Hashable {
    let id: String
    let slug: String
    let alternativeSlugs: [String: String]?
    let createdAt: String
    let updatedAt: String
    let promotedAt: String?
    let width: Int
    let height: Int
    let color: String
    let blurHash: String
    let description: String?
    let altDescription: String?
    let urls: Urls
    let links: Links
    let likes: Int
    let likedByUser: Bool
    let currentUserCollections: [String]?
    let sponsorship: String?
    //let topicSubmissions: [String: String]?
    let assetType: String
    let user: User?
    let tags: [Tag]
    
    var downloadedImage: UIImage?
    
    enum CodingKeys: String, CodingKey {
        case id, slug, alternativeSlugs = "alternative_slugs", createdAt = "created_at", updatedAt = "updated_at", promotedAt = "promoted_at", width, height, color, blurHash = "blur_hash", description, altDescription = "alt_description", urls, links, likes, likedByUser = "liked_by_user", currentUserCollections = "current_user_collections", sponsorship, assetType = "asset_type", user, tags
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    static func == (lhs: Photo, rhs: Photo) -> Bool {
        lhs.id == rhs.id
    }
}

// MARK: - User
struct User: Codable {
    let id: String
    let updatedAt: String
    let username: String
    let name: String
    let firstName: String?
    let lastName: String?
    let twitterUsername: String?
    let portfolioURL: String?
    let bio: String?
    let location: String?
    let links: UserLinks
    let profileImage: ProfileImage
    let instagramUsername: String?
    let totalCollections: Int
    let totalLikes: Int
    let totalPhotos: Int
    let totalPromotedPhotos: Int
    let totalIllustrations: Int
    let totalPromotedIllustrations: Int
    let acceptedTOS: Bool
    let forHire: Bool
    let social: Social
    
    enum CodingKeys: String, CodingKey {
        case id, updatedAt = "updated_at", username, name, firstName = "first_name", lastName = "last_name", twitterUsername = "twitter_username", portfolioURL = "portfolio_url", bio, location, links, profileImage = "profile_image", instagramUsername = "instagram_username", totalCollections = "total_collections", totalLikes = "total_likes", totalPhotos = "total_photos", totalPromotedPhotos = "total_promoted_photos", totalIllustrations = "total_illustrations", totalPromotedIllustrations = "total_promoted_illustrations", acceptedTOS = "accepted_tos", forHire = "for_hire", social
    }
}

// MARK: - UserLinks
struct UserLinks: Codable {
    let selfLink: String
    let html: String
    let photos: String
    let likes: String
    let portfolio: String
    let following: String
    let followers: String
    
    enum CodingKeys: String, CodingKey {
        case selfLink = "self"
        case html, photos, likes, portfolio, following, followers
    }
}

// MARK: - ProfileImage
struct ProfileImage: Codable {
    let small: String
    let medium: String
    let large: String
}

// MARK: - Social
struct Social: Codable {
    let instagramUsername: String?
    let portfolioURL: String?
    let twitterUsername: String?
    let paypalEmail: String?
    
    enum CodingKeys: String, CodingKey {
        case instagramUsername = "instagram_username"
        case portfolioURL = "portfolio_url"
        case twitterUsername = "twitter_username"
        case paypalEmail = "paypal_email"
    }
}

// MARK: - Urls
struct Urls: Codable {
    let raw: String
    let full: String
    let regular: String
    let small: String
    let thumb: String
    let smallS3: String
    
    enum CodingKeys: String, CodingKey {
        case raw, full, regular, small, thumb
        case smallS3 = "small_s3"
    }
}

// MARK: - Links
struct Links: Codable {
    let selfLink: String
    let html: String
    let download: String
    let downloadLocation: String
    
    enum CodingKeys: String, CodingKey {
        case selfLink = "self"
        case html, download
        case downloadLocation = "download_location"
    }
}

// MARK: - Tag
struct Tag: Codable {
    let type: String
    let title: String
    let source: TagSource?
}

// MARK: - TagSource
struct TagSource: Codable {
    let ancestry: Ancestry?
    let title: String
    let subtitle: String
    let description: String
    let metaTitle: String
    let metaDescription: String
    let coverPhoto: CoverPhoto
    
    enum CodingKeys: String, CodingKey {
        case ancestry, title, subtitle, description
        case metaTitle = "meta_title"
        case metaDescription = "meta_description"
        case coverPhoto = "cover_photo"
    }
}

// MARK: - Ancestry
struct Ancestry: Codable {
    let type: AncestryDetail
    let category: AncestryDetail?
    let subcategory: AncestryDetail?
    
    enum CodingKeys: String, CodingKey {
        case type, category, subcategory
    }
}

// MARK: - AncestryDetail
struct AncestryDetail: Codable {
    let slug: String
    let prettySlug: String
    
    enum CodingKeys: String, CodingKey {
        case slug
        case prettySlug = "pretty_slug"
    }
}

// MARK: - CoverPhoto
struct CoverPhoto: Codable {
    let id: String
    let slug: String
    let alternativeSlugs: [String: String]
    let createdAt: String
    let updatedAt: String
    let promotedAt: String?
    let width: Int
    let height: Int
    let color: String
    let blurHash: String
    let description: String?
    let altDescription: String?
    let urls: Urls
    let links: Links
    let likes: Int
    let likedByUser: Bool
    let currentUserCollections: [String]
    let sponsorship: String?
    //let topicSubmissions: [String: TopicSubmission]?
    let assetType: String
    let premium: Bool = false
    let plus: Bool = false
    let user: User
    
    enum CodingKeys: String, CodingKey {
        case id, slug, alternativeSlugs = "alternative_slugs", createdAt = "created_at", updatedAt = "updated_at", promotedAt = "promoted_at", width, height, color, blurHash = "blur_hash", description, altDescription = "alt_description", urls, links, likes, likedByUser = "liked_by_user", currentUserCollections = "current_user_collections", sponsorship, assetType = "asset_type", premium, plus, user
    }
}


