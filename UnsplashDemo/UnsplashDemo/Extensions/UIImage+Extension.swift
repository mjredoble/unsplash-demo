import UIKit

extension UIImage {
    func resizedToFitSquare(sideLength: CGFloat) -> UIImage? {
         // Calculate the scaling factor to maintain the aspect ratio
         let scale = min(sideLength / size.width, sideLength / size.height)
         
         // Calculate the new size after scaling
         let width = size.width * scale
         let height = size.height * scale
         
         // Determine the rect to center the image in the square
         let xOffset = (sideLength - width) / 2
         let yOffset = (sideLength - height) / 2
         let drawRect = CGRect(x: xOffset, y: yOffset, width: width, height: height)
         
         // Create a graphics context with the desired size
         UIGraphicsBeginImageContextWithOptions(CGSize(width: sideLength, height: sideLength), false, 0.0)
         
         // Fill the background with a transparent color
         UIColor.clear.setFill()
         UIRectFill(CGRect(x: 0, y: 0, width: sideLength, height: sideLength))
         
         // Draw the resized image
         draw(in: drawRect)
         
         // Get the resulting image
         let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
         
         // End the image context
         UIGraphicsEndImageContext()
         
         return resizedImage
     }
}
