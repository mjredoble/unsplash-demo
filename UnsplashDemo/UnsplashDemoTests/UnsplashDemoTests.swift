import XCTest
@testable import UnsplashDemo

final class UnsplashDemoTests: XCTestCase {
    func testJsonParsing() throws {
        guard let url = Bundle.main.url(forResource: "data", withExtension: "json") else {
            throw AppError(key: "Error testJsonParsing()", description: "Error testJsonParsing()")
        }
        
        let data = try Data(contentsOf: url, options: .mappedIfSafe)
        let searchPhotosDTO = try JSONDecoder().decode(SearchPhotosDTO.self, from: data)
        XCTAssert(searchPhotosDTO.results.count == 10)
    }
}
